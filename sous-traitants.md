---
title: "Sous-traitance des services"
description: 
keywords: Haïbu, SaaS, legal, sous-traitance
---

# Sous-traitance des services

Haïbu fait appel à de la sous-traitance pour assurer un bon niveau de service et pouvoir se concentrer sur ce qui fait sa force : la fourniture de services en ligne (*SaaS*).

Tous nos sous-traitants sont engagés à respecter la loi et la réglementation, soit par leurs CGV/CGU, soit par la signature de notre [contrat de sous-traitance](https://cloud.lydra.fr/s/iHNQMKrtGC8M8nD).

## Les missions de sous-traitance

| Catégorie   | Missions                               | Sous-traitants                                                           |
|-------------|----------------------------------------|--------------------------------------------------------------------------|
| Commercial  | Pages de vente et outil marketing      | [systeme.io](https://systeme.io/fr)                                      |
|             | Paiement en ligne                      | [Stripe](https://stripe.com/fr)                                          |
|             | Courriels                              | [6clones](https://www.6clones.fr)                                        |
| Hébergement | Services et données                    | [Scaleway](https://www.scaleway.com/fr)                                  |
|             | Code                                   | [Froggit](https://froggit.fr) et [Gitlab](https://gitlab.com)            |
|             | Courriels transactionnels              | [TipiMail](https://www.tipimail.com/)                                    |
|             | Sauvegardes                            | [Scaleway](https://www.scaleway.com/fr) et [OVH](https://www.ovh.com/fr) |
|             | Supervision (outils auto-hébergés)     | [OVH](https://www.ovh.com/fr)                                            |
|             | Supervision (page de statut)           | [UpDown.io](https://updown.io)                                           |
| Support     | Service                                | [Haïbu](https://lydra.fr/#team)                                                |
|             | Assistance utilisateurs (outils)       | [DWService](https://www.dwservice.net)                                   |
|             | Assistance utilisateurs (intervenants) | [Haïbu](https://lydra.fr/#team)                                                |
|             |                                        | [ézéo](https://ezeo.fr)                                                  |
