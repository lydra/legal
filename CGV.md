---
title: "Conditions Générales de Vente"
description: 
keywords: Haibu, SaaS, legal, CGV
---

# Conditions Générales de Vente

> Date de mise en application : 13/08/2021

## Préambule : qui sommes-nous ?
[Haïbu SCOP SAS](https://haibu.tech) est une Entreprise de Services Numériques (ESN) au format coopératif, dont le siège est situé au 173 rue Antoine Durafour 42100 Saint Étienne (RCS Saint Étienne, SIREN 939 290 664, capital social de 33000€ au 07/02/2025).

Les membres d'Haïbu ("nous" dans la suite du texte) sont animés par la coopération, les logiciels libres, ainsi que par l'autonomie et la souveraineté numérique. De ce fait, les Conditions Générales que vous, client ou futur client, êtes en train de lire se veulent compréhensibles par tout un chacun, en évitant le langage juridique jargonneux ⚖.

## Article 1 : champ d'application
Ces Conditions Générales de Vente (CGV) s'appliquent à tous les services en ligne (dits aussi "SaaS" pour *Software as a Service*) que nous proposons, à savoir les offres "[Froggit](http://froggit.fr)" et "[Boîte à Outils](https://www.lydra.eu/bao)".

## Article 2 : prix
Les services sont fournis aux tarifs affichés sur les sites de vente respectifs, au moment de la commande.

Les tarifs sont maintenus le temps de l'engagement mais peuvent être modifiés à chaque renouvellement, notamment avec l'évolution des services proposés qui pourront voir leurs fonctionnalités augmenter.

## Article 3 : engagement et rétractation
La commande est réalisée sur le site de vente avec paiement en ligne immédiat. Le renouvellement est tacitement reconduit à la fin de la période pour la même durée, le paiement est alors fait automatiquement.

Vous disposez d'un délai de rétractation 30 jours après la date du paiement, sans justification de votre part. La demande de remboursement se fait par courriel à l'adresse de support (voir [page des contacts](contacts.md)). Suite à votre rétractation, vous ne disposez plus de votre accès au service, il est donc à votre charge de récupérer l'intégralité de vos données **avant votre rétractation**.

En cas de modification des Conditions Générales de Vente, vous serez informés par courriel à minima un mois avant leur entrée en vigueur et disposerez d'un droit de rétractation du service en cas de refus des nouvelles conditions. Le refus et la rétractation devront être exprimés par courriel dans les quatres mois après la mise en application, voir notre [page des contacts](contacts.md).

## Article 4 : mise à disposition du service
Nous mettons à votre disposition le service commandé dans les meilleurs délais, dans un délai maximal de 5 jours ouvrés (du lundi au vendredi) sous réserve d'éventuels enregistrements DNS à réaliser par vos soins sur votre nom de domaine.

Les services sont fournis en l'état, et nous ne nous engageons pas à un taux précis de disponibilité. Cependant, nous mettons tout en œuvre, dans la mesure de nos moyens, pour que la qualité de service soit la meilleure possible, et remédier aux incidents dans les meilleurs délais.

Nous n'endosserons pas la responsabilité des dommages directs ou indirects en cas d'indisponibilité ou d'impossibilité d'accès aux services.

Nous ne sommes pas l'éditeur des logiciels mis en œuvre, mais l'hébergeur de ceux-ci. Nous ne serons donc pas tenus pour responsable en cas d'anomalies logicielles (*bugs*), même si cela entraîne la perte ou la corruption de données.

Les services proposés sont hébergés en France, basés sur des logiciels libres, et sauvegardés chaque jour sur des serveurs distants. Ces sauvegardes permettent la restauration des services uniquement en cas de panne majeure, vous n'y avez pas accès et toute demande de restauration à partir de ces sauvegardes techniques sera facturée en plus.

## Article 5 : support
Nous nous assurons du bon fonctionnement des services par de la supervision et du support. En cas de dysfonctionnement d'un service, vous pouvez ouvrir une demande au support via l'un des canaux proposés (voir [page des contacts](contacts.md)). Le support standard concerne la disponibilité et le bon fonctionnement du service uniquement.

La configuration et l'utilisation des outils sur le poste utilisateur ne fait pas partie du support standard, et nécessite la souscription d'un support utilisateur complémentaire (voir [page des services complémentaires](https://lydra.fr) pour obtenir une proposition).

Malgré tout, de la documentation (https://support.lydra.eu/help) est produite pour chaque service, sous forme de textes et/ou de vidéos, et accessible gratuitement.

## Article 6 : résiliation et litige
Vous disposez d'un délai de 30 jours pour vous rétracter. Au-delà vous êtes engagé pour la période commandée et ne pouvez exiger le remboursement de votre abonnement, même partiel. Les remboursements sont réalisés dans un délai de 14 jours ouvrés maximum.

En cas de retard de paiement, vous êtes redevable d'une pénalite de 10% de l'intégralité des sommes restant dues.

En cas de litige, vous êtes tenu d'exprimer votre demande par courriel à l'adresse de support (voir [page des contacts](contacts.md)) en expliquant votre grief). Si le traitement de votre demande ne vous donne pas satisfaction, vous êtes en droit de demander une médiation dont les modalités seront définies par le tribunal compétent.

## Article 7 : exclusion du service
Vous êtes libre d'utiliser les services comme bon vous semble, dans le respect des [Conditions Générales d'Utilisation](CGU.md), ainsi que de la loi et des réglementations en vigueur (en l'occurrence, la loi française et Européenne, ainsi que [le RGPD](https://www.cnil.fr/fr/reglement-europeen-protection-donnees)).

En cas de manquement constaté, nous nous réservons le droit de couper sans préavis vos accès aux services. Nous ne pourrons être tenus pour responsables des actions réalisées par nos clients et agirons dans le respect de la loi.

Dans ce cas, vous en serez informé aussitôt via les outils de communication que vous aurez donnés et ne pourrez demander le remboursement de votre abonnement.

[![licensebuttons by-sa](https://licensebuttons.net/l/by-sa/3.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0)
