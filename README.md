# Légal

Documents légaux des services d'Haïbu.

Ces documents sont sous Licence Libre, vous pouvez les réutiliser en modifiant vos informations et en nous créditant comme source.
[![licensebuttons by-sa](https://licensebuttons.net/l/by-sa/3.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0)
