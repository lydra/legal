---
title: "Politique de confidentialité"
description: 
keywords: Haïbu, SaaS, legal, Privacy
---

# Politique de confidentialité

> Date de mise en application : 13/08/2021

## Éthique
Haïbu ("nous" dans la suite du texte) propose des services en ligne permettant la coopération tout en respectant la vie privée. Ces services sont conçus à base de logiciels libres, sécurisés et hébergés en France afin de vous redonner confiance en vos outils numériques.

## Communication
La transparence est un engagement fort pour nous, aussi nous nous engageons à communiquer aussi souvent que nécessaire pour vous informer :
- de tout changement (documents légaux, évolutions des offres, nouvelles fonctionnalités, etc) ;
- des événements importants (perturbation forte du service, fuite de données, etc).

Vous pouvez à tout moment vous désabonner de nos canaux de communication (un lien systématique vous permettra de vous désinscrire), même si nous vous conseillons d'y rester pour ne pas rater d'information importante.

## Vos données
### Les données hébergées
Vos données vous appartiennent, et seulement à vous ! Nous nous interdisons de les utiliser pour d'autres finalités que la fourniture des services.

Les flux de communication pour accéder aux interfaces en ligne, ou pour la synchronisation via vos logiciels, sont sécurisés par du HTTPS (certificats TLS).

### Les données personnelles
Nous collectons des données personnelles pour vous proposer, fournir et améliorer nos services, mais aussi pour répondre à nos obligations légales.

| Type de données  | Données "personnelles"                                                                                                                                                                                                                        | Données contractuelles                                                                                                                                    |
|------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------|
| **commerciales** | - prénom et nom de la personne réalisant la commande<br/>- courriel et numéro de téléphone d'un contact<br/>- adresse postale<br/>- raison sociale (pour les entreprises et associations)<br/>- numéro de TVA intracommunautaire (le cas échéant) | - tarif de l'abonnement<br/>- périodicité<br/>- un moyen de paiement<br/>- date d’expiration du moyen de paiement<br/>- dates de début et de fin d’abonnement |
| **légales**      | Ces données sont extraites des logs :<br/>- horodatage des connexions<br/>- adresse IP<br/>- URL visitées                                                                                                                                        |                                                                                                                                                           |
| **techniques**   | - versions de navigateur et de système d'exploitation<br/>- versions des logiciels de synchronisation<br/>- consommation des ressources (espace disque, CPU, RAM)                                                                               |                                                                                                                                                           |
| **autres**       | - vos contributions sur les tchats, forums et autres canaux d’échange sont conservés même après votre désinscription                                                                                                                          |                                                                                                                                                           |

### Les sauvegardes
Les services et les données hébergées sont sauvegardés chaque nuit vers des serveurs externes à la plateforme de production, avec différents systèmes techniques. L'ensemble de nos sauvegardes sont chiffrées au départ du serveur de production, et donc illisible à quiconque y aurait accès sans autorisation.

Cette redondance de serveurs et de techniques nous permet d'assurer un niveau de sauvegarde sécurisant pour tout le monde, cependant gardez en tête qu'aucun système n'est infaillible.
C'est pour cela que nous vous conseillons aussi de mettre en place vos propres sauvegardes. Si vous avez besoin d'aide sur ce point nous pouvons vous proposer nos services.

## Hébergements
Nos services sont **exclusivement hébergés en France**, dans des sociétés respectant le RGPD et la législation en vigueur. Nos services sont donc en dehors de la législation américaine (cf. [CloudAct](https://fr.wikipedia.org/wiki/CLOUD_Act)).

Nous favorisons également les hébergeurs faisant des efforts d'amélioration de performances énergétiques, tout en restant dans des coûts nous permettant de proposer des services abordables à nos clients.

Nous mutualisons les serveurs pour héberger vos services, cela permet de réduire l'impact écologique et permet d'optimiser les ressources (moins de serveurs, moins de consommations directes et indirectes).

## Manipulation des données
Pour vous rendre les services, nous sommes amenés à manipuler vos données. Ces manipulations sont principalement liées aux fonctionnalités des logiciels que nous utilisons et sont automatisés. Mais nous pouvons être amenés à réaliser des opérations manuelles dans le cadre d'analyse ou de résolution de problème.

Dans tous les cas, vos données vous appartiennent et nous ne pourrons les utiliser en dehors de la fourniture des services.

## Sous-traitance
Afin de nous concentrer sur le développement de nos services, nous sous-traitons une partie des opérations techniques. L'ensemble de nos prestataires ou sous-traitants sont conformes au RGPD via leurs propres CGV/CGU ou ont signé notre [contrat de sous-traitance](https://cloud.lydra.fr/s/iHNQMKrtGC8M8nD).

Vous retrouverez la liste de nos sous-traitants et à quoi ils ont accès [à ce lien](sous-traitants.md).

## Sécurité
Nos serveurs sont sous Linux (logiciels libres) et configurés dans les règles de l'art pour assurer leur sécurité. C'est d'ailleurs un travail du quotidien pour améliorer cette protection.

Vos données sont chiffrées et les serveurs sont protégés par des firewalls réseau (anti-DDoS, etc) et serveur. Les serveurs et les services sont très régulièrement mis à jour afin de bénéficier des patchs de sécurité le plus vite possible.

Malgré tout nous ne pouvons vous garantir l'absence de failles de sécurité, notamment les [vulnérabilités "zero-day"](https://fr.wikipedia.org/wiki/Vulnérabilité_zero-day), car nous dépendons de la publication des correctifs des distributions Linux utilisées et des outils ou logiciels déployés.

## Rétention des données
Les **données légales** sont conservées pour une durée de 1 an, elles sont supprimées au-delà.

Les **données techniques** sont conservées en clair 14 jours. Ensuite elles sont supprimées.

Les **données commerciales** sont conservées le temps imposé par la loi[^conservationDonneesCommerciales]. Votre courriel peut rester aussi longtemps que vous le désirez dans nos listes de communication (un lien systématique vous permettra de vous désinscrire).
[^conservationDonneesCommerciales]: La loi impose une conservation des données nécessaires à la facturation pendant une durée de 10 ans minimum, et permet une conservation des autres données pendant 5 ans. ([source](https://www.economie.gouv.fr/entreprises/delai-conservation-documents))

Les **sauvegardes** des données des services sont conservées avec une durée variable selon le service et/ou le client, mais avec une durée maximale de 1 an.

## Accès aux données personnelles

Pour toute demande d'accès aux données personnelles, que ce soit pour les consulter, les modifier ou demander leur suppression, veuillez nous contacter (voir [page des contacts](contacts.md)).

## Cookies
La navigation sur nos sites et nos services peuvent déposer des cookies dont la seule finalité est la personnalisation des services et des interfaces, ou garder votre connexion active entre deux sessions. Nous n'utilisons pas les cookies des grandes régies publicitaires (Google, Facebook et cie) pour vous tracer ou analyser vos comportements, nos outils de statistiques sont auto-hébergés et en logiciels libres. Vous pouvez donc autoriser les cookies sans crainte pour vous faciliter la navigation sur nos services.

[![licensebuttons by-sa](https://licensebuttons.net/l/by-sa/3.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0)
