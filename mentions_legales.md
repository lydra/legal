---
title: "Mentions Légales"
description: 
keywords: Haïbu, SaaS, legal
---

# Mentions légales

## Identité

**[Haïbu SCOP SAS](https://haibu.tech)**

SOCIÉTÉ COOPÉRATIVE DE PRODUCTION EN SOCIÉTÉ PAR ACTIONS SIMPLIFIÉE – SCOP-SAS
SCOP créée en 2025
Capital social de 33000€ au 07/02/2025

Société inscrite au Registre du Commerce et des Sociétés de Saint Étienne sous le n° SIREN 939 290 664
N° de TVA intracommunautaire : FR82 939 290 664

Le siège social est situé à :

> Haïbu SCOP SAS
> 173 rue Antoine Durafour
> 42100 Saint Étienne
> France

📧 contact@haibu.tech
🌐 https://haibu.tech

Responsable de publication : Chaudier Christophe – contact@haibu.tech

## Hébergement du site
Ce site est un site statique hébergé sur les Froggit Pages.

Le service Froggit Pages est hébergé chez Scaleway 🇫🇷 :

> SCALEWAY SAS
> BP 438
> 75366 PARIS CEDEX 08
> FRANCE

🌐 www.scaleway.com
📞 +33 (0)1 84 13 00 00

## Liens hypertextes et cookies

Le site contient un certain nombre de liens hypertextes vers d’autres sites. Cependant, nous n’avons pas la possibilité de vérifier le contenu des sites ainsi visités, et nous n’assumerons en conséquence aucune responsabilité liée aux contenus qu’ils proposent.

La navigation sur le site est susceptible de provoquer l’installation de cookie(s) sur l’ordinateur de l’utilisateur. Un cookie est un fichier de petite taille, qui ne permet pas l’identification de l’utilisateur, mais qui enregistre des informations relatives à la navigation d’un ordinateur sur un site. Les données ainsi obtenues visent à faciliter la navigation ultérieure sur le site, et ont également vocation à permettre diverses mesures de fréquentation.

Le refus de dépôt d’un cookie peut entraîner l’impossibilité d’accéder à certains services. L’utilisateur peut toujours refuser l'utilisation des cookies grâce à la bannière dédiée.

[![licensebuttons by-sa](https://licensebuttons.net/l/by-sa/3.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0)
