---
title: "Page des contacts"
description: 
keywords: Haïbu, SaaS, legal, sous-traitance
---

# Page des contacts

## État des services
Vous pouvez consulter l'état de nos services sur les pages de statuts suivantes :
- https://status.froggit.fr

## Documentation et formations
Vous avez accès à notre documentation gratuite (https://support.lydra.eu/help) et aux FAQ de chaque service :
- Froggit : https://froggit.fr/faq
- Boîte à Outils : https://www.lydra.eu/bao#faq

## Autres demandes
Pour nous contacter, plusieurs options s'offrent à vous :
* pour vos questions commerciales
    * par courriel à [support@lydra.fr](mailto:support@lydra.fr)
* pour vos questions techniques
    * par courriel à [support@lydra.fr](mailto:support@lydra.fr)
    * par chat sur les canaux des services
        * [https://chat.froggit.fr/froggit/channels/support-clients](https://chat.froggit.fr/froggit/channels/support-clients)
        * [https://chat.froggit.fr/boite-a-outils/channels/support-clients](https://chat.froggit.fr/boite-a-outils/channels/support-clients)
* pour ce qui concerne le non-respect de la loi ou du droit d'auteur : par courriel à [abus@lydra.fr](mailto:abus@lydra.fr)
