---
title: "Conditions Générales d'Utilisation"
description: 
keywords: Haibu, SaaS, legal, CGU
---

# Conditions Générales d'Utilisation

> Date de mise en application : 13/08/2021

## Conditions d'accès aux services
Les services sont fournis tels quels et leur utilisation se fait sous votre responsabilité. Les services fournis ont vocation à être **en ligne et collaboratif**. Aussi vous comprenez que toute action de partage public de vos données entraîne de facto l'accès inconditionnel à celles-ci, et à n'importe qui, y compris les robots (*crawlers*) pour indexation par les moteurs de recherche.

Vous êtes responsable de la sécurisation de votre compte, dont votre mot de passe ou des autres systèmes de sécurisation ([2FA](https://fr.wikipedia.org/wiki/Double_authentification), [OTP](https://fr.wikipedia.org/wiki/Mot_de_passe_à_usage_unique), etc). Nous ne pourrons être tenus pour responsable de pertes ou dommages liés à une sécurisation trop faible de votre compte.

Nous ne nous engageons pas à un taux précis de disponibilité du service. Cependant nous mettons tout en œuvre, à la mesure de nos moyens, pour que la qualité de service soit la meilleure possible, et remédier aux incidents dans les meilleurs délais. Nous n'endosserons pas la responsabilité des dommages directs ou indirects en cas d'indisponibilité ou d'impossibilité d'accès aux services.
Nous ne sommes pas éditeur des logiciels mis en œuvre, mais hébergeur de ceux-ci. Nous ne serons donc pas tenus pour responsables en cas d'anomalies logicielles (*bugs*), même si cela entraîne la perte ou la corruption de données.

## Législation et propriété intellectuelle
Nous sommes uniquement fournisseurs de services et n'exerçons aucun contrôle sur les contenus et actions réalisés par ses usagers[^LCEN]. Vous êtes donc responsable de toute l’activité qui se produit avec votre compte (dont les contenus déposés et manipulés).
[^LCEN]: conformément à l’[article 6, I, 7 de la Loi n°2004-575](https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000042038977) pour la Confiance dans l’Economie Numérique du 21 juin 2004.

L'usage des services doit se faire dans le respect de la loi (française et Européenne) et des réglementations en vigueur ([RGPD](https://www.cnil.fr/fr/reglement-europeen-protection-donnees)). Vous devez aussi respecter la propriété intellectuelle, et nous vous encourageons d'ailleurs à publier sous une Licence Libre[^LicenceLibre].
[^LicenceLibre]: Vous référer à l'[article Wikipédia](https://fr.wikipedia.org/wiki/Licence_libre) pour comprendre ce qu'est une licence libre. Voici quelques exemples : [Creative Commons](https://creativecommons.org), [GPL](http://www.gnu.org/licenses/gpl-3.0.html), [BSD](https://en.wikisource.org/wiki/BSD_License), [Apache](https://www.apache.org/licenses/LICENSE-2.0.html), etc

En cas d'actions non conformes à la loi ou en cas de violation de la propriété intellectuelle, nous déclinons toute responsabilité et nous devons d'agir dans les meilleurs délais suite à tout signalement. Nous pouvons réaliser, potentiellement sans préavis, les actions suivantes selon la gravité, l'urgence et son appréciation propre : gel, clôture ou suppression du ou des comptes, avec notification a minima par courriel.

Pour signaler du contenu abusif ou un compte malveillant, veuillez écrire à l'adresse de signalement indiquée en [page des contacts](contacts.md).

## Libertés
Vous êtes libre d'utiliser les services fournis comme bon vous semble, tant que vous respectez les présentes [Conditions Générales d'Utilisation](CGU.md) et les [Conditions Générales de Vente](CGV.md).

Comme décrit dans notre [politique de confidentialité](confidentialite.md), les données déposées et manipulées sur les services **restent votre propriété**. Nous ne pourrons en aucun cas les utiliser à nos propres fins ni les diffuser à quiconque. Nous sommes seulement autorisés à manipuler vos données pour vous fournir les services auxquels vous avez souscrit et vous assister en cas d'anomalie.

## Données personnelles
En souscrivant à nos offres, vous consentez à ce que nous ou nos sous-traitants (voir la [liste des sous-traitants](sous-traitants.md)) collectent et utilisent certaines données à caractère personnel dans l'unique but de vous fournir les services. Conformément [aux articles 48 à 56](https://www.cnil.fr/fr/la-loi-informatique-et-libertes#article48) de la loi « Informatique et Libertés », vous disposez d'un droit d’opposition, d’accès et de rectification sur les données vous concernant. Vous pouvez exercer ce droit par simple courriel à l'adresse de support (voir [page des contacts](contacts.md)).

Des informations techniques pouvant contenir vos données personnelles sont aussi collectées automatiquement. Vous ne pouvez pas vous y opposer, car elles sont collectées soit par obligation légale, soit par les outils eux-mêmes afin de fournir le service et pour analyse en cas de problème technique.

La rétention de ces données personnelles est réduite au strict nécessaire (légal et/ou technique). La liste des données collectées est consultable dans notre [politique de confidentialité](confidentialite.md).

## Modification des services et des conditions d’utilisation
En cas de modification des Conditions Générales d'Utilisation, vous serez informés par courriel à minima un mois avant leur entrée en vigueur et disposerez d'un droit de rétractation du service en cas de refus des nouvelles conditions. Le refus et la rétractation devront être exprimés par courriel dans les quatres mois après la mise en application, voir notre [page des contacts](contacts.md).

## Localisation, sécurité et sauvegardes
Les services proposés sont hébergés en France, basés sur des logiciels libres, et sauvegardés chaque jour sur des serveurs distants. Ces sauvegardes permettent la restauration des services uniquement en cas de panne majeure, vous n'y avez pas accès et toute demande de restauration à partir de ces sauvegardes techniques sera facturée en plus.

[![licensebuttons by-sa](https://licensebuttons.net/l/by-sa/3.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0)
